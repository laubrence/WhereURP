package com.whereta.service;

import com.whereta.model.Account;
import com.whereta.vo.ResultVO;
import com.whereta.vo.UserCreateVO;
import org.apache.shiro.authz.SimpleAuthorizationInfo;

/**
 * @author Vincent
 * @time 2015/8/27 17:10
 */
public interface IAccountService {

    Account getAccountByAccount(String account);

    SimpleAuthorizationInfo getAccountRolePermission(int accountId);

    ResultVO selectAccount(int userId, int pageNow, int pageSize);

    ResultVO  saveUser(UserCreateVO createVO);

    ResultVO deleteUser(int userId);

    ResultVO updateUserDep(int userId,int depId);
}
