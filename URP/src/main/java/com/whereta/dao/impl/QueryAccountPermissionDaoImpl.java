package com.whereta.dao.impl;

import com.whereta.dao.IQueryAccountPermissionDao;
import com.whereta.mapper.QueryAccountPermissionMapper;
import com.whereta.model.QueryAccountPermission;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by vincent on 15-9-5.
 */
@Repository("queryAccountPermissionDao")
public class QueryAccountPermissionDaoImpl implements IQueryAccountPermissionDao{
    @Resource
    private QueryAccountPermissionMapper queryAccountPermissionMapper;
    @Override
    public QueryAccountPermission querySql(int id) {
        return queryAccountPermissionMapper.selectByPrimaryKey(id);
    }
}
