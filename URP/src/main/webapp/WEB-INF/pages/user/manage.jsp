<%--
  Created by IntelliJ IDEA.
  User: vincent
  Date: 15-9-3
  Time: 上午8:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<table id="main-user-manage-tb" fit="true"></table>
<div id="main-user-manage-add-dialog" title="添加用户" style="width:400px;height:auto;display: none;"
     data-options="iconCls:'icon-add',resizable:false,modal:true">

  <form method="post" id="main-user-manage-add-dialog-form">
    <table border="0" align="center">
      <tr>
        <td>分配部门</td>
        <td>
          <input  style="width:200px;height: 30px;" name="dep" data-options="required:true" />
        </td>
      </tr>
      <tr>
        <td>账号</td>
        <td>
          <input class="easyui-textbox" style="width:200px;height: 30px;" name="account" data-options="required:true,validType:'account[3,20]',missingMessage:'请输入账号'">
        </td>
      </tr>
      <tr>
        <td>密码</td>
        <td>
          <input class="easyui-textbox" style="width:200px;height:30px;" name="password" data-options="type:'password',required:true,validType:'password[6,20]',missingMessage:'请输入密码'">
        </td>
      </tr>
    </table>
  </form>
</div>

<div id="main-user-manage-edit-dep-dialog" title="修改部门" style="width:200px;height:300px;display: none;padding: 10px;"
     data-options="iconCls:'icon-add',resizable:false,modal:true">
  <ul id="main-user-manage-edit-dep-dialog_tree_deps"></ul>
</div>

<script>
  seajs.use(['user/manage'],function(m){
    m.init('${pageContext.request.contextPath}');
  });
</script>
</body>
</html>
